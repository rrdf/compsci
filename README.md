# CS
Repository of solutions worked on during college

<!-- ## Contents
    .
    ├── Awk                                  # Scripts for CS232 FOSS Lab
    ├── C 
    │   ├── CS100                            # Programs for Computer Programming
    │   ├── CS120                            # Programs for Computer Programming Lab
    │   ├── CS204                            # Programs for Operating Systems
    │   └── CS231                            # Programs for Data Structures Lab							
    ├── Java                                 # Programs for CS206 Object Oriented Design & Programming
    ├── LaTeX 
    │   ├── C Lab Exam Report 
    │   ├── Data Structures Lab Exam Report 
    │   ├── FOSS Lab Report 
    │   ├── Visual Pollution                 # HS210 Life Skills presentation
    │   └── Water Sensor for Tanks           # BE102 Design & Engineering presentation
    ├── Perl                                 # Script for CS232 FOSS Lab
    ├── PHP                                  # Script for CS232 FOSS Lab
    ├── Python 
    │   ├── CS110                            # Programs for Computer Science Workshop
    │   └── CS232                            # Program for CS232 FOSS Lab
    ├── Shell                                # Scripts for CS232 FOSS Lab
    └── VHDL                                 # Scripts for CS234 Digital System Lab -->

